package br.com.tisemcensura.fullstack.exceptions;

import java.io.Serializable;
import java.time.Instant;

public class StandartError implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Instant timestemp;
	private Integer status;
	private String error;
	private String messeger;
	private String path;
	
	public StandartError() {		
	}

	public StandartError(Instant timestemp, Integer status, String error, String messeger, String path) {
		super();
		this.timestemp = timestemp;
		this.status = status;
		this.error = error;
		this.messeger = messeger;
		this.path = path;
	}

	public Instant getTimestemp() {
		return timestemp;
	}

	public void setTimestemp(Instant timestemp) {
		this.timestemp = timestemp;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMesseger() {
		return messeger;
	}

	public void setMesseger(String messeger) {
		this.messeger = messeger;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	

}
